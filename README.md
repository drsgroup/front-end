# Desafio Front End

### Desafio:
O desafio consiste na criação de uma vitrine virtual consumindo a API Tmdb para listar os filmes populares.
[Tmbd](https://www.themoviedb.org/documentation/api)

- Você deverá exibir os filmes populares em uma lista
- Você deverá exibir os detalhes de cada filme listado em uma outra rota (:id)
- Você deverá criar uma documentação para justificar suas implementações. (/documentation)
- Você deverá realizar testes nos componentes criados.
- O design é livre, você pode usar sua criatividade para trazer uma boa experiência para o usuário

### Objetivos:

- Listar os filmes populares (Get Popular - Ref API).
- Disponibilizar os detalhes dos filmes listados.
- Documentar suas implementações e chamadas e justificar o porque utilizou de tal maneira.
- **O uso de rotas é obrigatório **

### Tecnologias/Padrões:
- Angular 7 
- SASS
- Qualquer Framework de estilo (Bootstrap / Materialize / Semantic UI)
- Teste Unitário (Escreva todos os testes que achar necessário)

### Critérios de Avaliação:
- Usabilidade
- Criatividade
- Código limpo e organização
- Documentação de código
- Documentação do projeto (readme)
- Performance

### Pré-requisitos para o consumo da Tmbd API:
- **API KEY**
- Para adquirir sua chave de api acesse: [Tmdb](https://www.themoviedb.org/)
- Crie uma conta/faça login
- Após logado acesse: [Tmdb Api Key](https://www.themoviedb.org/settings/api)

### Considerações
- Clone este repositório para a realização do teste.
- Você tem 2 dias para conclusão a partir do início que deverá ser informado via email.
- Seja claro nas suas implementações.
- Boa sorte!

